package com.exhibition.server.service

import com.exhibition.server.entity.Exhibition
import com.exhibition.server.entity.ExhibitionPojo
import com.exhibition.server.entity.Ticket
import com.exhibition.server.entity.TicketPojo
import org.json.simple.JSONObject
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import java.security.Principal

interface TicketsService {

    fun buyTicket(priceId: String, principal: Principal): ResponseEntity<JSONObject>

    fun loadTickets(pageable: Pageable, principal: Principal): Page<TicketPojo>
}