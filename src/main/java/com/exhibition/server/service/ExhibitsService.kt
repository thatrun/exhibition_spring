package com.exhibition.server.service

import com.exhibition.server.entity.Exhibit
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface ExhibitsService {

    fun loadExhibits(exhibitionId: String, pageable: Pageable): Page<Exhibit>
}