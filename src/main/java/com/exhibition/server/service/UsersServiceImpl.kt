package com.exhibition.server.service

import com.exhibition.server.entity.User
import com.exhibition.server.repository.UsersRepository
import org.json.simple.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class UsersServiceImpl: UsersService {

    @Autowired
    private lateinit var usersRepository: UsersRepository

    @Autowired
    private lateinit var authoritiesService: AuthoritiesService

    @Autowired
    private lateinit var userPasswordEncoder: PasswordEncoder

    //https://stackoverflow.com/questions/32019353/adding-a-custom-login-controller-with-spring-security/32028726
    override fun loadUserByUsername(p0: String?): UserDetails {
        val userDetails = usersRepository.findUserByEmail(p0?:"")
        if (userDetails.isPresent) {
            return userDetails.orElse(null)
        }
        throw UsernameNotFoundException(p0)
    }

    @Transactional
    override fun registerUser(email: String, password: String): ResponseEntity<JSONObject> {
        val user = User()
        val userNonEncodedPassword = password
        user.userPassword = userPasswordEncoder.encode(password)
        user.email = email
        user.accountNonExpired = true
        user.accountNonLocked = true
        user.enabled = true
        user.credentialsNonExpired = true
        val authority = authoritiesService.getAuthorityById(1)
        authority?.let {
            user.userAuthorities = hashSetOf(it)
        }

        val emailExistenceCheck = usersRepository.findUserByEmail(user.email).isPresent
        val json = JSONObject()
        return when {
            !emailExistenceCheck &&
                    userNonEncodedPassword.length >= 6 -> {
                try {
                    usersRepository.save(user)
                    json["message"] = "User successfully registered"
                    ResponseEntity.ok(json)
                } catch (t: Throwable) {
                    json["error"] = "Something went wrong"
                    json["message"] = t.message?:""
                    ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(json)
                }
            }
            emailExistenceCheck -> {
                json["error"] = "User with this email already exist"
                ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json)
            }
            userNonEncodedPassword.length < 6 -> {
                json["error"] = "Password length should be grater than 5"
                ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json)
            }
            else -> {
                json["error"] = "Something went wrong"
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(json)
            }
        }
    }
}