package com.exhibition.server.service

import com.exhibition.server.entity.Authority

interface AuthoritiesService {

    fun getAuthorityById(id: Long): Authority?
}