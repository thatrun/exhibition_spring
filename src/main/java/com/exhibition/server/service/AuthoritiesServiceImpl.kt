package com.exhibition.server.service

import com.exhibition.server.entity.Authority
import com.exhibition.server.repository.AuthoritiesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class AuthoritiesServiceImpl: AuthoritiesService {

    @Autowired
    private lateinit var authoritiesRepository: AuthoritiesRepository

    @Transactional
    override fun getAuthorityById(id: Long): Authority? = authoritiesRepository.findById(id).orElse(null)
}