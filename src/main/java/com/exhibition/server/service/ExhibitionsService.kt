package com.exhibition.server.service

import com.exhibition.server.entity.Exhibition
import com.exhibition.server.entity.ExhibitionPojo
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import java.security.Principal

interface ExhibitionsService {

    fun loadExhibitions(pageable: Pageable, principal: Principal): Page<ExhibitionPojo>
}