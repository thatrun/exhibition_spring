package com.exhibition.server.service

import com.exhibition.server.entity.Exhibit
import com.exhibition.server.repository.ExhibitsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class ExhibitsServiceImpl: ExhibitsService {

    @Autowired
    private lateinit var exhibitsRepository: ExhibitsRepository

    @PreAuthorize("hasAuthority('EXHIBITION_USER')")
    @Transactional(readOnly = true)
    override fun loadExhibits(exhibitionId: String, pageable: Pageable): Page<Exhibit> = exhibitsRepository.loadExhibitsForExhibition(exhibitionId, pageable)
}