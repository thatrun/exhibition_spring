package com.exhibition.server.service

import com.exhibition.server.entity.Exhibition
import com.exhibition.server.entity.ExhibitionPojo
import com.exhibition.server.repository.ExhibitionsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.security.Principal

@Service
open class ExhibitionsServiceImpl: ExhibitionsService {

    @Autowired
    private lateinit var exhibitionsRepository: ExhibitionsRepository

    @PreAuthorize("hasAuthority('EXHIBITION_USER')")
    @Transactional(readOnly = true)
    override fun loadExhibitions(pageable: Pageable, principal: Principal): Page<ExhibitionPojo> = exhibitionsRepository.loadExhibitions(principal.name, pageable)
}