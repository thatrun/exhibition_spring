package com.exhibition.server.service

import com.exhibition.server.entity.*
import com.exhibition.server.repository.PricesRepository
import com.exhibition.server.repository.TicketsRepository
import org.hibernate.Hibernate
import org.json.simple.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.security.Principal
import java.util.*

@Service
open class TicketsServiceImpl: TicketsService {

    @Autowired
    private lateinit var usersService: UsersService

    @Autowired
    private lateinit var pricesRepository: PricesRepository

    @Autowired
    private lateinit var ticketsRepository: TicketsRepository

    @Transactional
    @PreAuthorize("hasAuthority('EXHIBITION_USER')")
    override fun buyTicket(priceId: String, principal: Principal): ResponseEntity<JSONObject> {
        val price = pricesRepository.findById(priceId)
        Hibernate.initialize(price.orElse(Price()).tickets)
        val user = usersService.loadUserByUsername(principal.name) as? User?
        val existingTicket = ticketsRepository.findTicketByPriceAndUser(user?.userId?:"", price.orElse(Price()).priceId)
        val json = JSONObject()
        return when {
            price.isPresent && user != null && !existingTicket.isPresent && (price.orElse(Price()).tickets?.size?:0 < price.orElse(Price()).ticketsMaximumCount?:0 ||
                    price.orElse(Price()).ticketsMaximumCount == null) -> {
                val ticket = Ticket(user, price.orElse(Price()), Calendar.getInstance().timeInMillis)
                ticketsRepository.save(ticket)
                json["message"] = "Ticket created"
                ResponseEntity.ok(json)
            }
            existingTicket.isPresent -> {
                json["error"] = "Ticket already purchased"
                ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json)
            }
            !price.isPresent -> {
                json["error"] = "Price not found"
                ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json)
            }
            price.orElse(Price()).tickets?.size?:0 >= price.orElse(Price()).ticketsMaximumCount?:0 -> {
                json["error"] = "Tickets sold out"
                ResponseEntity.status(HttpStatus.BAD_REQUEST).body(json)
            }
            else -> {
                json["error"] = "Something went wrong"
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(json)
            }
        }
    }

    @Transactional
    @PreAuthorize("hasAuthority('EXHIBITION_USER')")
    override fun loadTickets(pageable: Pageable, principal: Principal): Page<TicketPojo> = ticketsRepository.findTicketByUserEmail(principal.name, pageable)
}