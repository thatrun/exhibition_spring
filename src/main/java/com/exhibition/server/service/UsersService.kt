package com.exhibition.server.service

import org.json.simple.JSONObject
import org.springframework.http.ResponseEntity
import org.springframework.security.core.userdetails.UserDetailsService
import javax.servlet.http.HttpServletRequest

interface UsersService: UserDetailsService {

    fun registerUser(email: String, password: String): ResponseEntity<JSONObject>
}