package com.exhibition.server.repository

import com.exhibition.server.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UsersRepository: JpaRepository<User, String> {

    @Query("select u from User u join fetch u.userAuthorities a where u.email = :username")
    fun findUserByEmail(@Param("username") username: String): Optional<User>
}