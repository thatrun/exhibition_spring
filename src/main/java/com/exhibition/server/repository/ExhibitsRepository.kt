package com.exhibition.server.repository

import com.exhibition.server.entity.Exhibit
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface ExhibitsRepository: JpaRepository<Exhibit, String> {

    @Query("select e from Exhibit e where e.exhibition.exhibitionId = :exhibitionId order by e.title")
    fun loadExhibitsForExhibition(@Param("exhibitionId") exhibitionId: String, pageable: Pageable): Page<Exhibit>
}