package com.exhibition.server.repository

import com.exhibition.server.entity.Exhibition
import com.exhibition.server.entity.Ticket
import com.exhibition.server.entity.TicketEmbeddedId
import com.exhibition.server.entity.TicketPojo
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface TicketsRepository: JpaRepository<Ticket, TicketEmbeddedId> {

    @Query("select t from Ticket t join t.user u join t.price p where u.userId = :userId and p.priceId = :priceId")
    fun findTicketByPriceAndUser(@Param("userId") userId: String, @Param("priceId") priceId: String): Optional<Ticket>

    @Query("select new com.exhibition.server.entity.TicketPojo(e) from Exhibition e left join e.price p left join p.tickets t left join t.user u where u.email = :email group by e.exhibitionId order by e.title")
    fun findTicketByUserEmail(@Param("email") email: String, pageable: Pageable): Page<TicketPojo>
}