package com.exhibition.server.repository

import com.exhibition.server.entity.Authority
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AuthoritiesRepository: JpaRepository<Authority, Long> {
}