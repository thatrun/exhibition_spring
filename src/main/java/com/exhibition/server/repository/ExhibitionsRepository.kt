package com.exhibition.server.repository

import com.exhibition.server.entity.Exhibition
import com.exhibition.server.entity.ExhibitionPojo
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface ExhibitionsRepository: JpaRepository<Exhibition, String> {

    @Query("select new com.exhibition.server.entity.ExhibitionPojo(e, u) from Exhibition e, User u left join e.price p left join p.tickets t left join t.user where u.email = :email group by e.exhibitionId order by e.title")
    fun loadExhibitions(@Param("email") email: String, pageable: Pageable): Page<ExhibitionPojo>
}