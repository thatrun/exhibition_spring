package com.exhibition.server.repository

import com.exhibition.server.entity.Price
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PricesRepository: JpaRepository<Price, String> {
}