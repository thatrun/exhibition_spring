package com.exhibition.server.config

import com.exhibition.server.config.encryption.Encoders
import com.exhibition.server.service.UsersService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.security.SecurityProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.core.annotation.Order
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.BeanIds
import org.springframework.social.connect.ConnectionFactoryLocator
import org.springframework.social.connect.UsersConnectionRepository
import org.springframework.social.connect.web.ProviderSignInController
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository




@Configuration("SecurityConfig")
@EnableWebSecurity
@Import(Encoders::class)
open class ServerSecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    private val userDetailsService: UsersService? = null

    @Autowired
    private val userPasswordEncoder: PasswordEncoder? = null

    /*@Autowired
    private val usersConnectionRepository: UsersConnectionRepository? = null

    @Autowired
    private val connectionFactoryLocator: ConnectionFactoryLocator? = null

    @Autowired
    private val facebookConnectionSignUp: FacebookConnectionSignUp? = null*/

    @Bean(name = [(BeanIds.AUTHENTICATION_MANAGER)])
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    open fun daoAuthenticationProvider(userDetailsService: UserDetailsService,
                                       userPasswordEncoder: PasswordEncoder): DaoAuthenticationProvider {
        val provider = DaoAuthenticationProvider()
        provider.setUserDetailsService(userDetailsService)
        provider.setPasswordEncoder(userPasswordEncoder)
        return provider
    }

    override fun configure(web: WebSecurity?) {
        web!!.ignoring()
                .antMatchers(HttpMethod.POST, "/users/register")
                .and()
                .ignoring()
    }

    override fun configure(http: HttpSecurity?) {
        http!!
                .csrf().disable()
                .anonymous().disable()
                .authorizeRequests()
                .antMatchers("/users/register").permitAll()
    }

    @Autowired
    @Throws(Exception::class)
    fun globalUserDetails(auth: AuthenticationManagerBuilder) {
        // @formatter:off
        auth.userDetailsService(userDetailsService).passwordEncoder(userPasswordEncoder)
    }



}