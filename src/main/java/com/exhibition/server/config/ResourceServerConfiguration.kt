package com.exhibition.server.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer



@Configuration
@EnableResourceServer
open class ResourceServerConfiguration: ResourceServerConfigurerAdapter() {

    private val RESOURCE_ID = "resource-server-rest-api"
    private val SECURED_READ_SCOPE = "#oauth2.hasScope('read')"
    private val SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')"
    private val SECURED_USERS_PATTERN = "/users/**"
    private val SECURED_TICKETS_PATTERN = "/tickets/**"
    private val SECURED_EXHIBITS_PATTERN = "/exhibits/**"
    private val SECURED_EXHIBITIONS_PATTERN = "/exhibitions/**"

    override fun configure(resources: ResourceServerSecurityConfigurer) {
        resources.resourceId(RESOURCE_ID)
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
                .requestMatchers()
                .antMatchers(SECURED_USERS_PATTERN).and().authorizeRequests()
                .antMatchers(HttpMethod.POST, SECURED_USERS_PATTERN).access(SECURED_WRITE_SCOPE)
                .anyRequest().access(SECURED_READ_SCOPE)
                .and()
                .requestMatchers()
                .antMatchers(SECURED_TICKETS_PATTERN).and().authorizeRequests()
                .antMatchers(HttpMethod.POST, SECURED_TICKETS_PATTERN).access(SECURED_WRITE_SCOPE)
                .anyRequest().access(SECURED_READ_SCOPE)
                .and()
                .requestMatchers()
                .antMatchers(SECURED_EXHIBITS_PATTERN).and().authorizeRequests()
                .antMatchers(HttpMethod.POST, SECURED_EXHIBITS_PATTERN).access(SECURED_WRITE_SCOPE)
                .anyRequest().access(SECURED_READ_SCOPE)
                .and()
                .requestMatchers()
                .antMatchers(SECURED_EXHIBITIONS_PATTERN).and().authorizeRequests()
                .antMatchers(HttpMethod.POST, SECURED_EXHIBITIONS_PATTERN).access(SECURED_WRITE_SCOPE)
                .anyRequest().access(SECURED_READ_SCOPE)
    }
}