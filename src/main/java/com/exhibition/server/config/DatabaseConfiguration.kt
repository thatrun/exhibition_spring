package com.exhibition.server.config

import com.google.auth.oauth2.AccessToken
import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.messaging.FirebaseMessaging
import org.apache.commons.dbcp2.BasicDataSource
import org.flywaydb.core.Flyway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.*
import org.springframework.core.env.Environment
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.web.filter.CharacterEncodingFilter
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import java.util.*
import javax.annotation.Resource
import javax.sql.DataSource
import org.springframework.web.multipart.commons.CommonsMultipartResolver
import org.springframework.context.annotation.Bean
import org.springframework.web.multipart.support.StandardServletMultipartResolver
import org.apache.tomcat.jni.SSL.setPassword
import org.springframework.mail.javamail.JavaMailSenderImpl
import org.springframework.mail.javamail.JavaMailSender
import org.dom4j.dom.DOMNodeHelper.setPrefix
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.ResourceLoader
import org.springframework.web.servlet.view.JstlView
import org.springframework.web.servlet.view.InternalResourceViewResolver
import org.springframework.web.servlet.ViewResolver
import java.io.FileInputStream


@Configuration
@EnableWebMvc
@EnableJpaRepositories("com.exhibition.server.repository")
@EnableTransactionManagement
@ComponentScan("com.exhibition.server")
@PropertySource("classpath:db.properties")
open class DatabaseConfiguration {

    private val PROP_DATABASE_DRIVER = "db.driver"
    private val PROP_DATABASE_PASSWORD = "db.password"
    private val PROP_DATABASE_URL = "db.url"
    private val PROP_DATABASE_PAC = "db.entity.package"
    private val PROP_DATABASE_USERNAME = "db.username"
    private val PROP_HIBERNATE_DIALECT = "hibernate.dialect"
    private val PROP_HIBERNATE_SHOW_SQL = "hibernate.show.sql"
    private val PROP_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto"

    @Resource
    private val env: Environment? = null

    @Bean
    open fun dataSource(): DataSource {
        val ds = BasicDataSource()
        ds.url = env?.getRequiredProperty("db.url")
        ds.driverClassName = env?.getRequiredProperty("db.driver")
        ds.username = env?.getRequiredProperty("db.username")
        ds.password = env?.getRequiredProperty("db.password")

        return ds
    }

    //https://flywaydb.org/documentation/migrations#sql-based-migrations
    @Bean(initMethod = "migrate")
    @Autowired
    open fun flyway(dataSource: DataSource): Flyway {
        val flyway = Flyway()
        flyway.setLocations("db/migration")
        flyway.isBaselineOnMigrate = true
        flyway.dataSource = dataSource
        return flyway
    }

    @Bean
    @DependsOn("flyway")
    open fun entityManagerFactory(): LocalContainerEntityManagerFactoryBean {
        val em = LocalContainerEntityManagerFactoryBean()
        em.dataSource = dataSource()
        em.setPackagesToScan(env?.getRequiredProperty("db.entity.package"))
        em.jpaVendorAdapter = HibernateJpaVendorAdapter()
        em.setJpaProperties(getHibernateProperties())

        return em
    }

    @Bean
    open fun multipartResolver(): StandardServletMultipartResolver {
        return StandardServletMultipartResolver()
    }

    @Bean
    open fun javaMailSender(): JavaMailSender {
        val mailSender = JavaMailSenderImpl()
        mailSender.host = "smtp.gmail.com"
        mailSender.port = 587

        mailSender.username = "gazanfarov.ruslan@gmail.com"
        mailSender.password = "9011800Birosix"

        val props = mailSender.javaMailProperties
        props["mail.transport.protocol"] = "smtp"
        props["mail.smtp.auth"] = "true"
        props["mail.smtp.starttls.enable"] = "true"
        props["mail.debug"] = "true"

        return mailSender
    }

    @Bean
    open fun characterEncodingFilter(): CharacterEncodingFilter {
        val characterEncodingFilter = CharacterEncodingFilter()
        characterEncodingFilter.encoding = "UTF-8"
        characterEncodingFilter.setForceEncoding(true)
        return characterEncodingFilter
    }

    @Bean
    open fun transactionManager(): PlatformTransactionManager {
        val manager = JpaTransactionManager()
        manager.entityManagerFactory = entityManagerFactory().`object`

        return manager
    }

    fun getHibernateProperties(): Properties {
        val properties = Properties()
        properties[PROP_HIBERNATE_DIALECT] = env?.getRequiredProperty(PROP_HIBERNATE_DIALECT)
        properties[PROP_HIBERNATE_SHOW_SQL] = env?.getRequiredProperty(PROP_HIBERNATE_SHOW_SQL)
        properties[PROP_HIBERNATE_HBM2DDL_AUTO] = env?.getRequiredProperty(PROP_HIBERNATE_HBM2DDL_AUTO)
        return properties
    }
}