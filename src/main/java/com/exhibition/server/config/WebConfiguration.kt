package com.exhibition.server.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.util.*
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer
import org.dom4j.dom.DOMNodeHelper.setPrefix
import org.springframework.web.servlet.view.InternalResourceViewResolver
import org.springframework.web.servlet.ViewResolver
import org.springframework.context.annotation.Bean

@Configuration
@EnableWebMvc
@ComponentScan("com.exhibition.server")
@EnableScheduling
open class WebConfiguration: WebMvcConfigurer {

    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>?) {
        val converter = MappingJackson2HttpMessageConverter()
        converter.objectMapper = ObjectMapper()
        converter.supportedMediaTypes = Collections.singletonList(MediaType.APPLICATION_JSON)

        converters?.add(converter)
    }

    override fun configureDefaultServletHandling(configurer: DefaultServletHandlerConfigurer?) {
        configurer!!.enable()
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry?) {
        registry!!.addResourceHandler("/resources/**")
                .addResourceLocations("/resource/js/", "/", "/js/")
    }

    @Bean
    open fun getViewResolver(): ViewResolver {
        val resolver = InternalResourceViewResolver()
        resolver.setPrefix("/WEB-INF/")
        resolver.setSuffix(".html")
        return resolver
    }
}