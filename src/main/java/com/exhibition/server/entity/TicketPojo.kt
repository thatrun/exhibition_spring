package com.exhibition.server.entity

class TicketPojo(exhibition: Exhibition) {

    var exhibitionId: String = exhibition.exhibitionId
    var title: String = exhibition.title
    var description: String = exhibition.description
    var image: String = exhibition.image
    var startTime: Long? = exhibition.startTime
    var price: Price? = exhibition.price
    var isTicketPurchased: Boolean = true

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ExhibitionPojo

        if (exhibitionId != other.exhibitionId) return false
        if (title != other.title) return false
        if (description != other.description) return false
        if (image != other.image) return false
        if (startTime != other.startTime) return false
        if (price != other.price) return false
        if (isTicketPurchased != other.isTicketPurchased) return false

        return true
    }

    override fun hashCode(): Int {
        var result = exhibitionId.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + image.hashCode()
        result = 31 * result + (startTime?.hashCode() ?: 0)
        result = 31 * result + (price?.hashCode() ?: 0)
        result = 31 * result + (isTicketPurchased?.hashCode() ?: 0)
        return result
    }
}