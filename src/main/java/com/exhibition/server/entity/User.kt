package com.exhibition.server.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonManagedReference
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "user")
class User: UserDetails, Serializable {

    @Id
    @Column(name = "user_id", unique = true, nullable = false, updatable = false, length = 40)
    var userId: String = UUID.randomUUID().toString()

    @Column(name = "enabled")
    var enabled: Boolean = false

    @Column(name = "email", nullable = false, updatable = true, unique = true, length = 50)
    var email: String = ""

    @Column(name = "credentials_expired")
    var credentialsNonExpired: Boolean = false

    @Column(name = "password", nullable = false)
    var userPassword: String = ""

    @Column(name = "account_expired")
    var accountNonExpired: Boolean = false

    @Column(name = "account_locked")
    var accountNonLocked: Boolean = false

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "users_authorities", joinColumns = [(JoinColumn(name = "user_id"))],
            inverseJoinColumns = [(JoinColumn(name = "authority_id"))])
    @OrderBy
    @JsonIgnore
    var userAuthorities: MutableSet<Authority> = HashSet()

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user",
            cascade = [(CascadeType.MERGE), (CascadeType.REMOVE)])
    @JsonManagedReference(value = "user_tickets")
    var tickets: Set<Ticket>? = null

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = userAuthorities

    override fun isEnabled(): Boolean = enabled

    override fun getUsername(): String = email

    override fun isCredentialsNonExpired(): Boolean = credentialsNonExpired

    override fun getPassword(): String = userPassword

    override fun isAccountNonExpired(): Boolean = accountNonExpired

    override fun isAccountNonLocked(): Boolean = accountNonLocked

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (userId != other.userId) return false
        if (enabled != other.enabled) return false
        if (email != other.email) return false
        if (credentialsNonExpired != other.credentialsNonExpired) return false
        if (userPassword != other.userPassword) return false
        if (accountNonExpired != other.accountNonExpired) return false
        if (accountNonLocked != other.accountNonLocked) return false

        return true
    }

    override fun hashCode(): Int {
        var result = userId.hashCode()
        result = 31 * result + enabled.hashCode()
        result = 31 * result + email.hashCode()
        result = 31 * result + credentialsNonExpired.hashCode()
        result = 31 * result + userPassword.hashCode()
        result = 31 * result + accountNonExpired.hashCode()
        result = 31 * result + accountNonLocked.hashCode()
        return result
    }


}