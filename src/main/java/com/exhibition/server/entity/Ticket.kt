package com.exhibition.server.entity

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "ticket")
class Ticket: Serializable {

    constructor()

    constructor(user: User, price: Price, addTime: Long) {
        this.user = user
        this.price = price
        this.addTime = addTime
        this.ticketId = TicketEmbeddedId(user.userId, price.priceId)
    }

    @EmbeddedId
    private var ticketId: TicketEmbeddedId = TicketEmbeddedId()

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    @JsonManagedReference(value = "user_tickets")
    var user: User? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("priceId")
    @JsonBackReference(value = "price_tickets")
    var price: Price? = null

    @Column(name = "add_time", nullable = false, updatable = true, length = 100)
    var addTime: Long = 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Ticket

        if (ticketId != other.ticketId) return false
        if (addTime != other.addTime) return false

        return true
    }

    override fun hashCode(): Int {
        var result = ticketId.hashCode()
        result = 31 * result + addTime.hashCode()
        return result
    }


}