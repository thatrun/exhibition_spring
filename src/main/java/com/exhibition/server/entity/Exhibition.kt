package com.exhibition.server.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonManagedReference
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "exhibition")
@JsonIgnoreProperties(value = ["exhibits"])
class Exhibition: Serializable {

    @Id
    @Column(name = "exhibition_id", unique = true, nullable = false, updatable = false, length = 40)
    var exhibitionId: String = UUID.randomUUID().toString()

    @Column(name = "title", nullable = false, updatable = true, length = 40)
    var title: String = ""

    @Column(name = "description", nullable = false, updatable = true, length = 300)
    var description: String = ""

    @Column(name = "image", nullable = false, updatable = true, length = 200)
    var image: String = ""

    @Column(name = "start_time", nullable = true, updatable = true)
    var startTime: Long? = 0

    @OneToOne(fetch = FetchType.LAZY, cascade = [(CascadeType.MERGE), (CascadeType.REMOVE)])
    @JoinColumn(name = "price_id", nullable = false)
    @JsonManagedReference(value = "exhibition_price")
    var price: Price? = null

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "exhibition")
    @JsonManagedReference(value = "exhibits")
    var exhibits: Set<Exhibit>? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Exhibition

        if (exhibitionId != other.exhibitionId) return false
        if (title != other.title) return false
        if (description != other.description) return false
        if (image != other.image) return false

        return true
    }

    override fun hashCode(): Int {
        var result = exhibitionId.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + image.hashCode()
        return result
    }


}