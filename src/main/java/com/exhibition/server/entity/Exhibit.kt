package com.exhibition.server.entity

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "exhibit")
@JsonIgnoreProperties(value = ["exhibition"])
class Exhibit {

    @Id
    @Column(name = "exhibit_id", unique = true, nullable = false, updatable = false, length = 40)
    var exhibitId: String = UUID.randomUUID().toString()

    @Column(name = "title", nullable = false, updatable = true, length = 40)
    var title: String = ""

    @Column(name = "image", nullable = false, updatable = true, length = 200)
    var image: String? = ""

    @Column(name = "description", nullable = false, updatable = true, length = 300)
    var description: String = ""

    @ManyToOne(fetch = FetchType.LAZY, cascade = [(CascadeType.MERGE)])
    @JoinColumn(name = "exhibition_id", nullable = false)
    @JsonBackReference(value = "exhibits")
    var exhibition: Exhibition? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Exhibit

        if (exhibitId != other.exhibitId) return false
        if (title != other.title) return false
        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        var result = exhibitId.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + description.hashCode()
        return result
    }


}