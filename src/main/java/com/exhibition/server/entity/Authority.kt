package com.exhibition.server.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "authority")
class Authority: GrantedAuthority, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "authority_id")
    var authorityId: Long = 0

    @Column(name = "name")
    var name: String = ""

    @ManyToMany(mappedBy = "userAuthorities")
    @JsonIgnore
    @Transient var users: Set<User>? = null//working on serializing exc

    override fun getAuthority(): String = name

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Authority

        if (authorityId != other.authorityId) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = authorityId.hashCode()
        result = 31 * result + name.hashCode()
        return result
    }

}