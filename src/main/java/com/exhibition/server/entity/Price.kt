package com.exhibition.server.entity

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonManagedReference
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "price")
@JsonIgnoreProperties(value = ["exhibition", "tickets"])
class Price: Serializable {
    @Id
    @Column(name = "price_id", unique = true, nullable = false, updatable = false, length = 40)
    var priceId: String = UUID.randomUUID().toString()

    @Column(name = "price_amount", nullable = false, updatable = true)
    var priceAmount: Double = 0.0

    @Column(name = "price_currency", nullable = false, updatable = true)
    var priceCurrency: String = ""

    @Column(name = "tickets_maximum_count", nullable = true, updatable = true)
    var ticketsMaximumCount: Int? = 0

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "price")
    @JoinColumn(name = "event_id", nullable = false)
    @JsonBackReference("exhibition_price")
    var exhibition: Exhibition? = null

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "price",
            cascade = [(CascadeType.MERGE), (CascadeType.REMOVE)])
    @JsonManagedReference(value = "price_tickets")
    var tickets: Set<Ticket>? = null//заинтересованные

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Price

        if (priceId != other.priceId) return false
        if (priceAmount != other.priceAmount) return false
        if (priceCurrency != other.priceCurrency) return false
        if (ticketsMaximumCount != other.ticketsMaximumCount) return false

        return true
    }

    override fun hashCode(): Int {
        var result = priceId.hashCode()
        result = 31 * result + priceAmount.hashCode()
        result = 31 * result + priceCurrency.hashCode()
        result = 31 * result + (ticketsMaximumCount ?: 0)
        return result
    }


}