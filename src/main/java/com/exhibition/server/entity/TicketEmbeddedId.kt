package com.exhibition.server.entity

import java.io.Serializable
import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class TicketEmbeddedId: Serializable {

    constructor()

    constructor(userId: String, priceId: String) {
        this.userId = userId
        this.priceId = priceId
    }

    @Column(name = "price_id", unique = true, nullable = false, updatable = false, length = 40)
    var priceId: String = UUID.randomUUID().toString()

    @Column(name = "user_id", unique = true, nullable = false, updatable = false, length = 40)
    var userId: String = UUID.randomUUID().toString()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TicketEmbeddedId

        if (priceId != other.priceId) return false
        if (userId != other.userId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = priceId.hashCode()
        result = 31 * result + userId.hashCode()
        return result
    }


}