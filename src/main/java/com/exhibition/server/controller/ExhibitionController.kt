package com.exhibition.server.controller

import com.exhibition.server.entity.Exhibition
import com.exhibition.server.entity.ExhibitionPojo
import com.exhibition.server.service.ExhibitionsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.security.Principal
import java.util.*

@RestController
@RequestMapping("/exhibitions")
class ExhibitionController {

    @Autowired
    private lateinit var exhibitionsService: ExhibitionsService

    @RequestMapping(method = [(RequestMethod.GET)], produces = ["application/json"])
    fun getPositions(@RequestParam("page") page: Optional<Int>,
                     @RequestParam("size") size: Optional<Int>, principal: Principal): Page<ExhibitionPojo> {
        val currentPage = page.orElse(1)
        val currentSize = size.orElse(5)
        return exhibitionsService.loadExhibitions(PageRequest.of(currentPage.minus(1), currentSize), principal)
    }
}