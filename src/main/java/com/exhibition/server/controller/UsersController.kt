package com.exhibition.server.controller

import com.exhibition.server.service.UsersService
import org.json.simple.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/users")
class UsersController {

    @Autowired
    private lateinit var usersService: UsersService

    @RequestMapping(value = ["/register"], method = [(RequestMethod.POST)], produces = ["application/json"])
    @ResponseStatus(value = HttpStatus.CREATED)
    fun registerUserAccount(@RequestParam("email") email: String,
                            @RequestParam("password") password: String): ResponseEntity<JSONObject> {
        return usersService.registerUser(email, password)
    }
}