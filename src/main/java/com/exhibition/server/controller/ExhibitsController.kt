package com.exhibition.server.controller

import com.exhibition.server.entity.Exhibit
import com.exhibition.server.entity.Exhibition
import com.exhibition.server.service.ExhibitsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/exhibits")
class ExhibitsController {

    @Autowired
    private lateinit var exhibitsService: ExhibitsService

    @RequestMapping(method = [(RequestMethod.GET)], produces = ["application/json"])
    fun getPositions(@RequestParam("exhibitionId") exhibitionId: String,
                     @RequestParam("page") page: Optional<Int>,
                     @RequestParam("size") size: Optional<Int>): Page<Exhibit> {
        val currentPage = page.orElse(1)
        val currentSize = size.orElse(5)
        return exhibitsService.loadExhibits(exhibitionId, PageRequest.of(currentPage.minus(1), currentSize))
    }
}