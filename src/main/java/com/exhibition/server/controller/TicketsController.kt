package com.exhibition.server.controller

import com.exhibition.server.entity.Exhibition
import com.exhibition.server.entity.ExhibitionPojo
import com.exhibition.server.entity.Ticket
import com.exhibition.server.entity.TicketPojo
import com.exhibition.server.service.TicketsService
import org.json.simple.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.security.Principal
import java.util.*

@RestController
@RequestMapping("/tickets")
class TicketsController {

    @Autowired
    private lateinit var ticketsService: TicketsService

    @RequestMapping(value = ["/add"], method = [(RequestMethod.PUT)], produces = ["application/json"])
    fun addTicket(@RequestParam("priceId") priceId: String, principal: Principal): ResponseEntity<JSONObject> =
            ticketsService.buyTicket(priceId, principal)

    @RequestMapping(method = [(RequestMethod.GET)], produces = ["application/json"])
    fun getPositions(@RequestParam("page") page: Optional<Int>,
                     @RequestParam("size") size: Optional<Int>,
                     principal: Principal): Page<TicketPojo> {
        val currentPage = page.orElse(1)
        val currentSize = size.orElse(5)
        return ticketsService.loadTickets(PageRequest.of(currentPage.minus(1), currentSize), principal)
    }
}