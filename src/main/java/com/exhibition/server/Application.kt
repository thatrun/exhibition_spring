package com.exhibition.server

import com.exhibition.server.config.ServerSecurityConfig
import com.exhibition.server.config.WebConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.WebApplicationInitializer
import org.springframework.web.context.ContextLoaderListener
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext
import org.springframework.web.filter.DelegatingFilterProxy
import org.springframework.web.servlet.DispatcherServlet
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import javax.servlet.MultipartConfigElement
import javax.servlet.ServletContext

@Configuration
@EnableWebMvc
@EnableScheduling
open class Application: WebApplicationInitializer {
    private val DISPATCHER = "dispatcher"
    private val TMP_FOLDER = "/tmp"
    private val MAX_UPLOAD_SIZE = (5 * 1024 * 1024).toLong()

    override fun onStartup(servletContext: ServletContext?) {
        val ctx = AnnotationConfigWebApplicationContext()
        ctx.register(WebConfiguration::class.java)
        ctx.register(ServerSecurityConfig::class.java)
        servletContext?.addListener(ContextLoaderListener(ctx))
        val servletRegistration = servletContext?.addServlet(DISPATCHER, DispatcherServlet(ctx))
        servletRegistration?.addMapping("/")
        servletRegistration?.setLoadOnStartup(1)

        //https://stackoverflow.com/questions/33130156/insufficientauthenticationexception-there-is-no-client-authentication-try-addi
        val filter = DelegatingFilterProxy("springSecurityFilterChain")
        filter.contextAttribute = "org.springframework.web.servlet.FrameworkServlet.CONTEXT.dispatcher"
        servletContext?.addFilter("springSecurityFilterChain", filter)?.addMappingForUrlPatterns(null, false, "/*")

        val multipartConfigElement = MultipartConfigElement(TMP_FOLDER,
                MAX_UPLOAD_SIZE, MAX_UPLOAD_SIZE * 2, MAX_UPLOAD_SIZE.div(2).toInt())
        servletRegistration?.setMultipartConfig(multipartConfigElement)
    }
}